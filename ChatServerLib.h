/*****************************************************************************/
/*  Author:            Bohdan D. Jacklitsch                                  */
/*  Creation Date:     11/10/2020                                            */
/*  Last Updated:      11/24/2020                                            */
/*  Due Date:          11/24/2020                                            */
/*  Course:            CSC328 - Network Programming                          */
/*  Professor Name:    Dr. Lisa Frye                                         */
/*  Assignment:        Final Project                                         */
/*  Filename:          client.c                                              */
/*  Language:          C                                                     */
/*  Compiler:          g++                                                   */
/*  Execution Command: ./a.out                                               */
/*  Purpose:           This file acts as a client to a chat message server,  */
/*                     so it connects to the server (whose IP is provided as */
/*                     a command-line argument), enters a nickname, and can  */
/*                     send and recieve messages to and from the server.     */
/*****************************************************************************/
//TODO
//Must enter exit to exit
//NICE TO HAVES
//

//As usual, no clue what most of these do or if they are necessary
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include "ChatServerLib.h"
#include <pthread.h>
#include <errno.h>
#include <signal.h>

//Maximum size of message portion
#define CHATSIZE 10
//Maximum length of nickname
#define MAXNICK 32
//Default port number to connect to server
#define DEFAULTPORT 1337
//Maximum size of message portion
#define MESSAGESIZE 1078

extern int errno;
typedef struct{
    int socketDescriptor;
}subthreadData;

/* 
DESCRIPTION
    userArgumentChecker checks for the number of arguments: if 2, it returns the
	default port number if it is 3, it returns the 3rd arguement as the port
	number, and if it is any other number, it gives a usage clause and exits.

PARAMETERS
    int argc is the number of arguments passed by the user
    char **argv are the actual arguements passed by the user, used to determine
		whether the correct number of arguments are passed and what the port
		number should be
             
RETURN
    Returns integer, namely the port number of the server
*/
int userArgumentChecker(int argc, char **argv);

/* 
DESCRIPTION
    interpretServerAddress formats the server's address based on the
	hostname and port number

PARAMETERS
    char *inputServer is the hostname, as entered by the user
    struct sockaddr_in **serverAddress is a pointer to a variable that stores
		information on the server's address, used here as an "out" parameter
	int portNumber is the port number for the server, as determined by
		userArgumentChecker()
             
RETURN
    Returns nothing, as it completes on success, exits on failure
*/
void interpretServerAddress(char *inputServer, struct sockaddr_in **serverAddress, int portNumber);

void runClient(struct sockaddr_in **serverAddress);

/* 
DESCRIPTION
    setupClient creates a socket and connects it to the server, exiting on
	failure

PARAMETERS
    struct sockaddr_in **serverAddress is a pointer to a variable that stores
		information on the server's address, used here as an "out" parameter
             
RETURN
    Returns integer, namely the socket file descriptor of the connection
*/
int setupClient(struct sockaddr_in **serverAddress);

/* 
DESCRIPTION
    confirmConnection receives a response from the server upon connection,
	receiving a proper protocol if all goes well, and exits if it does not

PARAMETERS
    int socketDescriptor stores the socket file descriptor of the connection
		to the server
             
RETURN
    Returns nothing, as it completes on success, exits on failure
*/
void confirmConnection(int socketDescriptor);

/* 
DESCRIPTION
    selectNickname asks the user to type in a nickname and sends it to the
	server if it meets requirements, which in turn either says that the
	nickname is already in use and retries or accepts it and continues

PARAMETERS
    int socketDescriptor stores the socket file descriptor of the connection
		to the server
             
RETURN
    Returns char*, the pointer to the nickname string
*/
char* selectNickname(int socketDescriptor);

/* 
DESCRIPTION
    getChat is the function that a thread spawns in to constantly prints
	received chats and exits the program if the server sends bye

PARAMETERS
    void *receiveRequirements is a void pointer, meant to be converted to abort
		subthreadData struct, which in turn has an integer which stores the
		socket file descriptor of the connection to the server
        
RETURN
    Returns a void pointer, which is probably something that threads need I guess 
*/
void *getChat(void *receiveRequirements);

//Main does main things
int main(int argc, char **argv)
{
	int portNumber;
	int socketDescriptor; //Socket file descriptor for the client
	struct sockaddr_in *serverAddress; //Address for the server
	
	portNumber = userArgumentChecker(argc, argv); //Do not continue program if user has incorrect number of arguments

	interpretServerAddress(argv[1], &serverAddress, portNumber); //Convert the server address provided by the user to the correct server
	
	runClient(&serverAddress); //Get quote using TCP protocol
	
	return 0;
}

//Checks the number of arguments the user put, giving a usage clause or setting port number as needed
int userArgumentChecker(int argc, char **argv)
{
	if (argc == 2) //Three arguments: command, protocol, and hostname
    {
		return (DEFAULTPORT);
    }
	else if (argc == 3) //Three arguments: command, protocol, and hostname
    {
		return (atoi(argv[2]));
    }
	else
	{
      printf("\nusage: %s <hostname> [server port]\n", argv[0]);
      exit(1);
	}
}

//May not be necessary entirely, but formats the server address properly
void interpretServerAddress(char *inputServer, struct sockaddr_in **serverAddress, int portNumber)
{
	struct addrinfo *serverInfo = (struct addrinfo *)malloc(sizeof(struct addrinfo)); //Allocate memory for addrinfo for server details
	
	int returnHolder; //Stores return value of function/process for evaluation
	
	memset(serverInfo, 0, sizeof(serverInfo));
	serverInfo->ai_family = AF_INET; //As we are using an Internet Standard, seems reasonable that it would work with just the internet
	serverInfo->ai_flags = 0; //No flags that I am aware of
	serverInfo->ai_protocol = IPPROTO_TCP; //Might be redundant, but restricts protocol to only TCP.
	serverInfo->ai_socktype = SOCK_STREAM; //To make the socket TCP
	
	returnHolder = getaddrinfo(inputServer, 0, serverInfo, &serverInfo);
	if (returnHolder < 0) //If error
	{
		printf("Error in getaddrinfo call: %s\n", gai_strerror(returnHolder));
		exit(-1);
	}
	
	*serverAddress = (struct sockaddr_in *)serverInfo->ai_addr; //Extract address
	(*serverAddress)->sin_port = htons(portNumber); //Make Port number to RFC 865 standard
	
	free(serverInfo); //Free memory of addrinfo, as server info was interpreted
}

void runClient(struct sockaddr_in **serverAddress)
{  
	bool initiateClose = false;
	char byeCommand[5] = "EXIT";
	subthreadData receiveRequirements;
	
	char *messageString=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store quote from the server to print
	messageString[0] = '\0'; //Just in case, initialize string to be (effectively) empty
	
	char *nickString=(char *)malloc(sizeof(char)*MAXNICK); //Store quote from the server to print
	
	int threadStatus; //Used to check whether thread is working correctly
	pthread_t threadReference; //"Holds" the created thread
	
	int socketDescriptor = setupClient(serverAddress);
	confirmConnection(socketDescriptor);
	nickString = selectNickname(socketDescriptor);
	
	receiveRequirements.socketDescriptor = socketDescriptor;
	printf("Passed\n");
	threadStatus = pthread_create(&threadReference, NULL, getChat, &receiveRequirements);
	if (threadStatus)
	{
	printf("ERROR creating threads; return code from pthread_create() is %d.\nError message:%s\n", threadStatus, strerror(errno));
	
		pthread_cancel(threadReference);
		exit(-1);
	}
	
	while (!initiateClose)
	{
		initiateClose = true;
		printf("%s: ",nickString);
		fgets(messageString, CHATSIZE, stdin);
		printf("%s\n",messageString);

		if ((strlen(messageString) > 0) && (messageString[strlen (messageString) - 1] == '\n'))
			messageString[strlen (messageString) - 1] = '\0';
	
		for (int currentCharacter = 0; messageString[currentCharacter] != '\0', byeCommand[currentCharacter] != '\0'; currentCharacter++)
		{
			if (toupper(messageString[currentCharacter]) != toupper(byeCommand[currentCharacter]))
			{
				initiateClose = false;
				break;
			}
		}
		if (!initiateClose)
		{
			printf("Here idiot\n");
			if (sendMessage(socketDescriptor, CHAT, nickString, messageString, MAXNICK, CHATSIZE) <= 0) //If message is not sent by the client to server
			{
				perror("Error sending message to server");
				
				close(socketDescriptor); //Close socket
				free(messageString); //Free memory allocated to store the incoming quot
				exit(-1);
			}
		}
	}
	
	if (sendMessage(socketDescriptor, BYE, NULL, NULL, 0, 0) <= 0) //If message is not sent by the client to server
	{
		perror("Error sending message to server");
		
		close(socketDescriptor); //Close socket
		free(messageString); //Free memory allocated to store the incoming quot
		exit(-1);
	}
	free(messageString); //Free memory allocated to store the incoming quote
	
	close(socketDescriptor); //Close socket
}

//Sockets and Connects, returning socket descriptor
int setupClient(struct sockaddr_in **serverAddress)
{  
	
	int returnHolder; //Stores return value of function/process for evaluation
	
	int socketDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); //Create a socket for the client
	if (socketDescriptor < 0)
	{
		perror("Error creating socket: ");
		exit(-1);
	}
	returnHolder = connect(socketDescriptor, (struct sockaddr *) *serverAddress, sizeof(struct sockaddr)); //Connect to server
	if (returnHolder == -1) //If error
	{
		perror("Error connecting to server: ");
		if (errno == 111) //Error number for "Connection refused"
		{
			printf("This may mean that the server is not running, at least not on this port.\n");
		}
		exit(-1);
	}
	return socketDescriptor;
}

//Checks if received "HELLO" protocol from server on connection
void confirmConnection(int socketDescriptor)
{
	char *messageString=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store quote from the server to print
	messageString[0] = '\0'; //Just in case, initialize string to be (effectively) empty
	
	struct messageInfo messageSpecs;
	
	if (receiveMessage(socketDescriptor, messageString, MESSAGESIZE) > 0)
	{
		getInfo(&messageSpecs, messageString);
		if (messageSpecs.protocol != HELLO)
		{
			printf("Server is experiencing difficulties: Server did not send connection confirmation\n");
			free(messageString); //Free memory allocated to store the incoming quote
			exit(-1);
		}
	}
	else
	{
		perror("Error receiving message from server");
		
		close(socketDescriptor); //Close socket
		free(messageString); //Free memory allocated to store the incoming quote
		exit(-1);
	}
	free(messageString); //Free memory allocated to store the incoming quote
}

//Asks user for nickname, looping until correct and unique nickname is selected
char* selectNickname(int socketDescriptor)
{
	bool nickAccepted; //True if nickname was accepted by the server
	char *nickString=(char *)malloc(sizeof(char)*MAXNICK); //Store quote from the server to print
	
	
	char *messageString=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store quote from the server to print
	messageString[0] = '\0'; //Just in case, initialize string to be (effectively) empty
	
	struct messageInfo messageSpecs;
	
	do
	{
		do
		{
			nickAccepted = true; //Innocent until proven guilty while loop approach
			printf("Enter what you would like your nickname to be. Please do not use spaces.\n");
			fgets(nickString, MAXNICK, stdin); //Fricking gets nickString, up to MAXNICK length

			if ((strlen(nickString) > 0) && (nickString[strlen (nickString) - 1] == '\n'))
				nickString[strlen (nickString) - 1] = '\0'; //Gotta love C - replaces newline with null terminator as terminating character
			
			for (int currentCharacter = 0; nickString[currentCharacter] != '\0'; currentCharacter++) 
			{
				if (isspace(nickString[currentCharacter]) != 0) //Gives an issue if any of the characters are spaces
				{
					printf("Please do not use spaces, try again.\n");
					nickAccepted = false; //Proven guilty
					break;
				}
			}
		}
		while(!nickAccepted);
			
		if (sendMessage(socketDescriptor, NICK, nickString, NULL, MAXNICK, 0) <= 0) //If message is not sent by the client to server
		{
			perror("Error sending message to server");
			
			close(socketDescriptor); //Close socket
			free(messageString); //Free memory allocated to store the incoming quot
			exit(-1);
		}
		
		if (receiveMessage(socketDescriptor, messageString, MESSAGESIZE) > 0) //If any message is sent by the server to the client
		{
			getInfo(&messageSpecs, messageString);
			if (messageSpecs.protocol == READY) //READY
			{
				printf("Nickname accepted, welcome %s!\n", nickString);
			}
			else if (messageSpecs.protocol == RETRY) //RETRY
			{
				printf("Sorry, but that nickname is already in use. Please try another.\n");
				nickAccepted = false;
			}
			else
			{
				printf("Server is experiencing difficulties: Nickname not sent properly\n");
				printf("Got protocol %d\n", messageSpecs.protocol);
				free(messageString); //Free memory allocated to store the incoming quote
				exit(-1);
			}
		}
		else
		{ 
			perror("Error recieving message from server");
			
			close(socketDescriptor); //Close socket
			free(messageString); //Free memory allocated to store the incoming quote
			exit(-1);
		}
	}
	while (!nickAccepted);
	free (messageString);
	return nickString;
}
 
//Subthread which just prints chat messages and closes if server sends BYE
void *getChat(void *receiveRequirements)
{
	char *messageString=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store quote from the server to print
	messageString[0] = '\0'; //Just in case, initialize string to be (effectively) empty
	
    //Way of extracting structs credited to qwr
    subthreadData *lreceiveRequirements = (subthreadData*)receiveRequirements; //Typecasts from void to subthreadData
	int socketDescriptor = lreceiveRequirements->socketDescriptor; //Gets the socket descriptor from struct
	
	struct messageInfo messageSpecs; //Used for library getInfo parsing
	
	while (true)
	{
		if (receiveMessage(socketDescriptor, messageString, MESSAGESIZE) > 0) //If any message is sent by the server to the client
		{
			getInfo(&messageSpecs, messageString);
			if (messageSpecs.protocol == BYE) //Server shutting down
			{
				printf("Server is shutting down shortly, disconnecting.\n");
				close(socketDescriptor); //Close socket
				free(messageString); //Free memory allocated to store the incoming quote
				exit(-1);
			}
			else
			{
				if (messageSpecs.msg != "") //Don't print an empty message
					printf("%s: %s\n", messageSpecs.name, messageSpecs.msg);
			}
		}
		else
		{ 
			perror("Error recieving message from server");
			close(socketDescriptor); //Close socket
			free(messageString); //Free memory allocated to store the incoming quote
			exit(-1);
		}
		messageString[0] = '\0';
	}
	free(messageString); //Free memory allocated to store the incoming quote
    pthread_exit(NULL);
}
